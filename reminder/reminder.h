#define TITLE_L 32
#define INFO_L 128

typedef struct task {
  int date;
  char title[TITLE_L];
  char info[INFO_L];
}task;

typedef struct event_node {
  struct task *task;
  struct event_node *next; 
}event;

void addEvent(event * head, task * t) {
  event * ptr = head;
  event * tmp;
  while( ptr->next != NULL && cmpDate(ptr->next->task , t) == -1 ) ptr=ptr->next;
  tmp = ptr->next;
  ptr->next = (event *) malloc(sizeof(event));
  ptr=ptr->next;
  ptr->next = tmp;
  ptr->task = t;
}

//if t1 happens before t2 return -1
//if t1 happens after t2 return 1
int cmpDate(task * t1, task * t2) {
  if(t1->date < t2->date) return -1;
  else return 1;
}

void printEvents(event * head) {
  event * ptr = head;
  ptr = ptr->next;
  while(ptr != NULL) {
    printf("%s -> ", ptr->task->title);
    ptr = ptr->next;
  }
  printf("NULL\n");
}

int writetofile(event * head, FILE * fout) {
--  fwrite
}
