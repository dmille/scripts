#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "reminder.h"

int main (int argc, char * argv[]) {
  char title[TITLE_L];
  int date;
  task * tk;
  event * head = (event *) malloc (sizeof(event));
  

  head->next = NULL;
  for(;;) {
    printf("Enter event title: ");
    scanf("%s", &title);
    printf("Enter date: ");
    scanf("%d", &date);
    tk = (task *) malloc (sizeof(task));
    strcpy(tk->title, title);
    tk->date = date;
    addEvent(head,tk);
    printEvents(head);
  }
  return 0;
}
