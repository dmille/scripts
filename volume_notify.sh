#!/bin/bash
NID=42
EXPR=`amixer -M | grep Mono.*[\d] | cut -d [ -f2 | sed s/%]//g`
dir=`pwd`
old=`cat $dir/.volume`
vol=$EXPR

if [[ $vol -ne $old ]]
then
    /usr/local/bin/notify-send -t 100 -p -r $NID -u critical "Volume: $vol% "
    echo $vol > `pwd`/.volume
fi
