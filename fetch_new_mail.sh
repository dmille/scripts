#!/bin/bash

emailnum=( $(fetchmail -c | cut -d ' ' -f 1) )

if ! [[ ${emailnum[0]} =~ ^f.* ]]
then
    DISPLAY=:0.0 /usr/bin/notify-send "${emailnum[0]} new mail for personal "
fi
if ! [[ ${emailnum[1]} =~ ^f.* ]]
then
    DISPLAY=:0.0 /usr/bin/notify-send "${emailnum[1]} new mail for school "
fi
fetchmail
