#!/bin/bash

MUTE=`amixer | grep M.*\dB | cut -d "[" -f4 | sed s/"]"//`
NID=41
if [[ $MUTE == "on" ]]
then
	/usr/local/bin/notify-send -t 100 -r $NID -u critical "unmute"
else
	/usr/local/bin/notify-send -t 1000 -r $NID -u critical "mute"
fi
