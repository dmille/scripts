#!/bin/bash 

BATTINFO=`acpi -b`
if [[ `echo $BATTINFO | grep Discharging` && `echo $BATTINFO | cut -f 5 -d " "` < 00:15:00 ]] ; then
	DISPLAY=:0.0 /usr/bin/notify-send "NIGGA PLUG ME IN --- " "$BATTINFO"
	if [[ `echo $BATTINFO | cut -f 5 -d " "` < 00:10:00 ]] ; then
		NID=$(/usr/local/bin/notify-send -p "Shutting down in 2 minutes")
		echo "Shutting down at $(date)" >> /home/david/bin/.suspend.log
		sleep 60;
		CNT=60
		until [ $CNT -eq 0 ]; do
			/usr/local/bin/notify-send -r $NID "$CNT seconds till suspend"
			let CNT-=1
			sleep 1
		done
		if [[ `acpi -b | grep Discharging` ]] ; then
			/usr/sbin/pm-suspend
		fi
	fi
fi
