#!/bin/bash

mkdir negated
cp $1 negated
cd negated
pdftoppm -png $1 $2
for i in `ls *.png`
do convert ${i%.*}.png -negate ${i%.*}.png
done