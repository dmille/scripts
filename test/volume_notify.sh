#!/bin/bash
NID=42
EXPR=`amixer | grep Mono.*[\d] | cut -d [ -f2 | sed s/%]//g`

VOL=$EXPR
echo $VOL
if [[ $1 == "up" && $VOL -lt 100 ]]
then
	pactl set-sink-volume 0 +2%
else
if [[ $1 == "down" && $VOL -gt 0 ]]
then
	pactl set-sink-volume 0 -- -2%
fi
/usr/local/bin/notify-send -t 100 -r $NID -u critical "Volume: $VOL% "
